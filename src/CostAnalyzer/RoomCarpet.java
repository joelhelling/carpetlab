/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CostAnalyzer;

/**
 *
 * @author joel.helling904
 */
public class RoomCarpet {
    private RoomDimension size;
    private double carpetCost;
    
    public RoomCarpet(RoomDimension dim, double cost)
    {
        if( dim == null )
        {
            System.out.println("Using Default Room Dimension of (1,1)");
            this.size = new RoomDimension(1,1);
        }
        else
            this.size = dim;
        
        this.carpetCost = cost;
    }
    
    public double getTotalCost()
    {
        return this.size.getArea() * this.carpetCost;
    }
    
    public String toString()
    {
        return "Cost of Carpet=" + this.carpetCost + 
               "\nDimension=" + size.toString();
    }
}
