/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CostAnalyzer;
import java.util.*;
import java.text.DecimalFormat;
/**
 *
 * @author joel.helling904
 */
public class CarpetDriver {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double length = 0.0;
        double width = 0.0;
        double cost = 0.0;
        RoomDimension dim = null;
        RoomCarpet carpet = null;
        DecimalFormat dec = new DecimalFormat("$###,###.##");
        
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Please Enter the Cost per Square Foot of the Carpet in Dollars: ");
        cost = scan.nextDouble();
        System.out.println("Please Enter the Dimensions of the Room: ");
        System.out.print("Length in Feet: ");
        length = scan.nextDouble();
        System.out.print("Width in Feet: ");
        width = scan.nextDouble();
        
        dim = new RoomDimension(length,width);
        carpet = new RoomCarpet(dim,cost);
        
        System.out.println("The Total Cost is " + dec.format(carpet.getTotalCost()) );
        
        
    }
}
